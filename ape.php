<?php 
require_once 'animal.php';
    class Ape extends Binatang
    {
        public $legs = 2;
        public $yell = "Auoooo";
    }
    $kera = new Ape("Kera Sakti");
    echo "Name : ". $kera->name;
    echo "<br>";
    echo "Legs : ". $kera->legs;
    echo "<br>";
    echo "Cold Blooded : ". $kera->cold_blooded;
    echo "<br>";
    echo "Jump : ". $kera->yell;
?>