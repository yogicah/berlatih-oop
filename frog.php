<?php 
require_once 'animal.php';
    class Frog extends Binatang
    {
        public $jump = "hop hop";
    }
    $katak = new Frog("Buduk");
    echo "Name : ". $katak->name;
    echo "<br>";
    echo "Legs : ". $katak->legs;
    echo "<br>";
    echo "Cold Blooded : ". $katak->cold_blooded;
    echo "<br>";
    echo "Jump : ". $katak->jump;
?>